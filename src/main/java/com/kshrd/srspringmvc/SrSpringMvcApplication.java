package com.kshrd.srspringmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SrSpringMvcApplication {

    public static void main(String[] args) {
        SpringApplication.run(SrSpringMvcApplication.class, args);
    }

}
