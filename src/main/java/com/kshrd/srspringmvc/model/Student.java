package com.kshrd.srspringmvc.model;


import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Student {
    private int id;

    @NotEmpty(message = "Name cannot be empty..")
// @Size(min = 5,max = 6,message = "min is 5 max 6 ")
    private String name;

    @NotEmpty(message = "Address cannot be empty..")
    private String address;
    // MM/YY
    @NotEmpty(message = "Birthdate must follow this format DD/MM/YY")
    @Pattern(regexp = "^(0[1-9]|1[0-9]|2[0-9]|3[0-1])(\\/)(0[1-9]|1[0-2])(\\/)([1-9][1-9])$")
    private String birthdate ;

    private String profile;
    @NotEmpty(message = "You must choose gender. .")
    private String gender;


}
