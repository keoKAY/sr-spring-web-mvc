package com.kshrd.srspringmvc.controller;

import com.kshrd.srspringmvc.model.Student;
import com.kshrd.srspringmvc.service.FileStorageService;
import com.kshrd.srspringmvc.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;

@Controller
public class StudentController {

    @Autowired
  @Qualifier("studentServiceImp")

    StudentService studentService;
    // 3 methods : field, setter, constructor

    @Autowired
    FileStorageService fileStorageService;

    @GetMapping
    public String index(Model model){

//        System.out.println("here is the value of all the students :");
//       studentService.getAllStudents().stream().forEach(System.out::println);
//
        model.addAttribute("student", new Student());
        model.addAttribute("students",studentService.getAllStudents());
        return "index";


    }


    @GetMapping("/form-add")
    public String showFormAdd(Model model){

        model.addAttribute("student",new Student());
        return "form-add";
    }

    @PostMapping("/handle-add")
    public String handleAdd(@ModelAttribute @Valid Student student, @RequestParam("file") MultipartFile file, BindingResult bindingResult){

        try{
            String filename = "http://localhost:8080/images/"+fileStorageService.saveFile(file);
            System.out.println("filename: "+filename);
            student.setProfile(filename);

        }catch (IOException ex){
            System.out.println("Error with the imaage upload "+ex.getMessage());
        }


        if (bindingResult.hasErrors()){
            return "/form-add";
        }
//        System.out.println("Value of student : "+student);
        studentService.AddStudentMethod(student);

      //  return "index";

      return  "redirect:/";
    }



//    View Student By Id

    @GetMapping("/view-student/{id}")
    public String viewStudent(@PathVariable int id,Model model){
        // Find Student By ID

        Student resultStudent = studentService.findStudentByID(id);

        System.out.println("result student : "+resultStudent);
        // send value to the view
        model.addAttribute("student",resultStudent);



        return "form-view";
    }
}
